package com.manoj.gc.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KubernateTestController {
	
	@GetMapping("/app")
	public String getMessate() {
		
		return "kubernate welcome";
	}
}
